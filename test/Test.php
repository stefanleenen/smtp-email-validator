<?php

namespace stefanleenen\Test;

use stefanleenen\Validator;
use PHPUnit_Framework_TestCase;

class Test extends PHPUnit_Framework_TestCase {

    protected $instance;
    protected $from = 'test@example.com';

    protected function setUp() {
        $this->instance = new Validator($this->from);
    }

    protected function tearDown() {
        unset($this->instance);
    }

    public function testResultArray() {

        $results = $this->instance->validate(['bla']);

        $this->assertInternalType('array',$results);
        $this->assertEquals(1, count($results));
        $this->assertArrayHasKey('bla', $results);
    }

//    public function testValidEmail() {
//        $is_valid = $this->instance->validate('support@github.com');
//        $this->assertTrue($is_valid);
//    }
//
//    public function testInvalidEmail() {
//        $is_valid = $this->instance->validate('non-existent-email@github.com');
//        $this->assertFalse($is_valid);
//    }
}