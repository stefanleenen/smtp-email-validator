<?php

namespace stefanleenen;

use Zend\Validator\EmailAddress;

/**
 * Class Validator
 * @package stefanleenen
 */
class Validator {

    /**
     * @var string
     */
    private $from = 'test@example.com';

    /**
     * @var int
     */
    private $port = 25;

    /**
     * @var int
     */
    private $connection_timeout = 5;

    /**
     * @var int
     */
    private $max_read_time = 10;

    /**
     * @var bool
     */
    private $debug = false;

    /**
     * @var array
     */
    private $emails = [];

    /**
     * @var array
     */
    private $results = [];

    /**
     * @param null $from
     */
    public function __construct($from = null) {

        if ($from != null) {
            $this->from = $from;
        }
    }

    /**
     * @param array $emails
     * @return array
     */
    public function validate(array $emails) {

        $this->emails = $emails;

        $this->results = [];

        foreach($this->emails as $email) {

            // 1. Test if $email is a valid email string
            $is_valid_string = $this->isValidEmailString($email);

            if (!$is_valid_string) {
                $this->addResult($email, false, 'Not a valid email string');
                continue;
            }

            // 2. Get MX records for domain
            $hosts = $this->getMXRecords($email);

            if (empty($hosts)) {
                $this->addResult($email, false, 'No MX records found');
                continue;
            }

            // 3. Query the MX server(s)
            $this->checkMXForEmail($email, $hosts);

        }


        return $this->results;

    }

    /**
     * @param $email
     * @param $hosts
     */
    private function checkMXForEmail($email, $hosts) {

        $socket = false;

        // Try to establish a connection with one of the MX records
        while(list($host) = each($hosts)) {

            if ($socket = fsockopen($host, $this->port, $errno, $errstr, $this->connection_timeout)) {
                stream_set_timeout($socket, $this->max_read_time);
                break;
            }
        }

        if ($socket === false) {
            $this->addResult($email, false, 'Could not open a connection to any of the MX servers');
            return;
        }

        // We have an open socket.
        // Wait for the server, this might take a few seconds, depending on the server, but we want a 'greeting' first
        $greeting = fgets($socket);

        if (preg_match('/^220 /', $greeting) != 1) {
            $this->addResult($email, false, 'Got an unexpected greeting from the MX server');
            fclose($socket);
            return;
        }

        // Send HELO
        $helo = "HELO " . $this->getFromDomain();
        $reply = $this->sendAndGetReply($socket, $helo);

        if (preg_match('/^250 /', $reply) != 1) {
            $this->addResult($email, false, 'HELO response was not 250');
            fclose($socket);
            return;
        }

        // Send MAIL FROM:
        $from_msg = "MAIL FROM: <" . $this->from . ">";
        $reply = $this->sendAndGetReply($socket, $from_msg);

        if (preg_match('/^250 /', $reply) != 1) {
            $this->addResult($email, false, 'MAIL FROM response was not 250');
            fclose($socket);
            return;
        }

        // Send RCPT TO:
        $rcpt_msg = "RCPT TO: <" . $email . ">";
        $reply = $this->sendAndGetReply($socket, $rcpt_msg);

        preg_match('/^([0-9]{3}) /ims', $reply, $matches);
        $code = isset($matches[1]) ? $matches[1] : '';

        switch($code) {

            case "250":
                $this->addResult($email, true, 'Email is valid');
                break;

            case "451":
            case "452":
                $this->addResult($email, true, 'Got greylisted response, assume email is valid');
                break;

            default:
                $this->addResult($email, false, 'Mailbox not found');

        }

        fclose($socket);
        return;

    }

    /**
     * @return string
     */
    private function getFromDomain() {

        return substr(strstr($this->from, '@'), 1);

    }

    /**
     * @param $socket
     * @param $msg
     * @return string
     */
    private function sendAndGetReply($socket, $msg) {

        fwrite($socket, $msg."\r\n");

        $reply = fgets($socket);

        return $reply;
    }

    /**
     *
     * Get the MX records for a domain, no windows support for now.
     *
     * TODO: Build in cache, so we don't do useless dns requests
     *
     * @param $email
     * @return array
     */
    private function getMXRecords($email) {

        $hosts = [];
        $mxweights = [];

        $domain = substr(strstr($email, '@'), 1);

        if (function_exists('getmxrr')) {
            getmxrr($domain, $hosts, $mxweights);
        }

        $mxs = array_combine($hosts, $mxweights);
        asort($mxs, SORT_NUMERIC);

        return $mxs;
    }

    /**
     * @param $email
     * @param $valid
     * @param $msg
     */
    private function addResult($email, $valid, $msg) {

        $this->results[$email] = [
            'valid' => $valid,
            'message' => $msg,
        ];

    }

    /**
     * @param $email
     * @return bool
     */
    private function isValidEmailString($email) {

        // Sanity checking
        $email_check = new EmailAddress();

        return $email_check->isValid($email);

    }

    /**
     * @param $value
     */
    public function setDebug($value) {

        $this->debug = boolval($value);

    }

}